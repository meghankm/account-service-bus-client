package com.meghan.accountconfigbusclient.model;

import java.math.BigDecimal;

public class Account {

    private Integer accountId;
    private String accountType;
    private BigDecimal balance;
    private String dailyLimit;
    private String dbPort;

    public Account() {
    }

    public Account(Integer accountId, String accountType, BigDecimal balance) {
        this.accountId = accountId;
        this.accountType = accountType;
        this.balance = balance;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(String dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public String getDbPort() {
        return dbPort;
    }

    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }
}
